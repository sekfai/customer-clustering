from bokeh.io import output_file, save, show
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import DataTable, TableColumn, NumberFormatter

def plot(data):
    # output_file("table.html")
    source = ColumnDataSource(data)
    columns = [
        TableColumn(field="category", title="category".replace('_', ' ').title()),
        TableColumn(field="average_recency", title="Average Days Since Last Purchase"),
        TableColumn(field="average_frequency", title="Average Frequency (days)"),
        TableColumn(field="number_of_customers", title="number_of_customers".replace('_', ' ').title(), formatter=NumberFormatter(format='0,0')),
    ]
    data_table = DataTable(source=source, columns=columns, width=700, height=200)
    data_table.row_headers = False
    data_table.fit_columns = True
    # save(data_table)
    return data_table
    # show(data_table)