import psycopg2
import pandas as pd


class Database:
    user = "xx"
    password = "xx"
    name = "xx"
    SQL_1 = "drop table property;"
    SQL_2 = """create table property ("mapLat" numeric, "mapLon" numeric,
                                      seller varchar, "ownerName" varchar,
                                      "isOwner" boolean, "ownerPhone" varchar,
                                      "askingPrice" numeric, "Bedrooms" varchar,
                                      "Bathrooms" numeric, "Property_Type" varchar,
                                      "Tenure" varchar, "Land_Area" varchar,
                                      "Facing_Direction" varchar, "Occupancy" varchar,
                                      "Furnishing" varchar, "Posted_Date" varchar,
                                      "Built_Up" varchar, "listingID" numeric,
                                      state varchar, area varchar,
                                      township varchar, "stateCode" varchar,
                                      built_up int, furnishing int,
                                      predicted numeric
                                     );"""
    SQL_3 = """insert into property values (%s, %s, %s, %s, %s,
                                            %s, %s, %s, %s, %s,
                                            %s, %s, %s, %s, %s,
                                            %s, %s, %s, %s, %s,
                                            %s, %s, %s, %s, %s);"""

    def __init__(self, property_data):
        self.connection = psycopg2.connect(user = Database.user,
                                           password = Database.password,
                                           dbname = Database.name)
        self.cursor = self.connection.cursor()
        self.property_data = property_data

    def drop_table_property(self):
        try:
            self.cursor.execute(Database.SQL_1)
            self.connection.commit()
            print ('Successfully drop the table "property"')
        except psycopg2.DatabaseError as e:
            self.connection.rollback()
            print ("Error: {}".format(e))

    def create_table_property(self):
        try:
            self.cursor.execute(Database.SQL_2)
            self.connection.commit()
            print ('Successfully create the table "property"')
        except psycopg2.DatabaseError as e:
            self.connection.rollback()
            print ("Error: {}".format(e))
    
    def insert_record_to_property(self):
        for index, row in self.property_data.iterrows():
            data = (row['mapLat'], row['mapLon'],
                    row['seller'], row['ownerName'],
                    row['isOwner'], row['ownerPhone'],
                    row['askingPrice'], row['Bedrooms'],
                    row['Bathrooms'], row['Property_Type'],
                    row['Tenure'], row['Land_Area'],
                    row['Facing_Direction'], row['Occupancy'],
                    row['Furnishing'], row['Posted_Date'],
                    row['Built_Up'], row['listingID'],
                    row['state'], row['area'],
                    row['township'], row['stateCode'],
                    row['built_up'], row['furnishing'],
                    row['predicted'])
            self.cursor.execute(Database.SQL_3, data)
            self.connection.commit()

    def close_connection(self):
        self.connection.close()
        print ('Disconnected from "{}"'.format(Database.name))


def run():
    property_data = pd.read_csv('property_data.csv')
    db = Database(property_data)
    db.drop_table_property()
    db.create_table_property()
    db.insert_record_to_property()
    db.close_connection()


if __name__ == "__main__":
    run()
