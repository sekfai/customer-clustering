import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

def KMeans_clustering(data, index_col, features, n_clusters=6): 
    """ clustering using the standardized data(not PCA data)
    Parameters:
        data: DataFrame
        features: columns in data denote the variables for clustering, list of string
    """
    df = data.set_index(index_col)
    x = df.loc[:][features]
    x_std = StandardScaler().fit_transform(x)
    kmeans = KMeans(n_clusters=n_clusters, init='k-means++', n_init=10)
    df['label'] = kmeans.fit_predict(x_std)
    
    reduced_data = PCA(n_components=2).fit_transform(x_std)
    df['x'] = reduced_data[:,0]
    df['y'] = reduced_data[:,1]
    
    return df.reset_index()