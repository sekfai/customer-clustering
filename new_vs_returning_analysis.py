import os
import pandas as pd
from django.conf import settings


def download_csv(filename, data):
    data = data.sort_values(by='Total', ascending=False)
    data.to_csv(os.path.join(settings.MEDIA_ROOT, filename), index=False)

def download_excel(filename, new, returning):
    writer = pd.ExcelWriter(os.path.join(settings.MEDIA_ROOT, filename))
    new.to_excel(writer, 'New', index=False)
    returning.to_excel(writer, 'Returning', index=False)
    writer.save()

def brand_pivot_table(data):
    pivot_table = data.pivot(index='Brand', columns='customer_status', values='revenue').fillna(value=0)
    pivot_table['Total'] = pivot_table['New'] + pivot_table['Returning']
    pivot_table = pivot_table.sort_values(by='Total')
    return pivot_table.reset_index()

def category_pivot_table(data):
    pivot_table = data.pivot(index='Category', columns='customer_status', values='revenue').fillna(value=0)
    pivot_table['Total'] = pivot_table['New'] + pivot_table['Returning']
    pivot_table = pivot_table.sort_values(by='Total')
    return pivot_table.reset_index()

def product_pivot_table(data):
    pivot_table = data.pivot(index='Sku', columns='customer_status', values='revenue').fillna(value=0)
    pivot_table['Total'] = pivot_table['New'] + pivot_table['Returning']
    pivot_table = pivot_table.sort_values(by='Total')
    return pivot_table.reset_index()
