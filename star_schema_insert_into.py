dim_customer = """
insert into dim_customer (original_id, email) values (%(original_id)s, %(email)s)
on conflict on constraint dim_customer_original_id_key do nothing;
"""

dim_order = """
insert into dim_order (original_id) values (%s)
on conflict on constraint dim_order_original_id_key do nothing;
"""

dim_order_status = """
insert into dim_order_status (status) values (%s)
on conflict on constraint dim_order_status_status_key do nothing;
"""

dim_time = """
insert into dim_time
(year, day_of_year, quarter, month, month_name, day_of_month, week, day_of_week, cal_date)
select
    date_part('year', date(%(order_date)s)) as year,
    date_part('doy', date(%(order_date)s)) as day_of_year,
    date_part('quarter', date(%(order_date)s)) as quarter,
    extract(month from date(%(order_date)s)) as month,
    to_char(date(%(order_date)s), 'Month') as month_name,
    date_part('day', date(%(order_date)s)) as day_of_month,
    extract(week from date(%(order_date)s)) as week,
    extract(dow from date(%(order_date)s)) as day_of_week,
    date(%(order_date)s) as cal_date
on conflict on constraint dim_time_cal_date_key do nothing;
"""

dim_product = """
insert into dim_product (original_id) values (%s)
on conflict on constraint dim_product_original_id_key do nothing;
"""

dim_brand = """
insert into dim_brand (brand) values (%s)
on conflict on constraint dim_brand_brand_key do nothing;
"""

dim_category = """
insert into dim_category (category) values (%s)
on conflict on constraint dim_category_category_key do nothing;
"""

dim_sku = """
insert into dim_sku (sku) values (%s)
on conflict on constraint dim_sku_sku_key do nothing;
"""

dim_ga_transaction = """
insert into dim_ga_transaction (original_id) values (%s)
on conflict on constraint dim_ga_transaction_original_id_key do nothing;
"""

dim_source = """
insert into dim_source (source) values (%s)
on conflict on constraint dim_source_source_key do nothing;
"""

dim_medium = """
insert into dim_medium (medium) values (%s)
on conflict on constraint dim_medium_medium_key do nothing;
"""

dim_campaign = """
insert into dim_campaign (campaign) values (%s)
on conflict on constraint dim_campaign_campaign_key do nothing;
"""

fact_order = """
insert into fact_order (customer_id,
                        order_id,
                        order_status_id,
                        time_id,
                        product_id,
                        brand_id,
                        category_id,
                        sku_id,
                        ga_transaction_id,
                        qty,
                        subtotal)
select
    dim_customer.id,
    dim_order.id,
    dim_order_status.id,
    dim_time.id,
    coalesce(dim_product.id, 0),
    coalesce(dim_brand.id, 0),
    coalesce(dim_category.id, 0),
    coalesce(dim_sku.id, 0),
    coalesce(dim_ga_transaction.id, 0),
    orderline.quantity,
    orderline.quantity * orderline.price
from
    (select
        %(customer_id)s::varchar as customer_id,
        %(order_id)s::varchar as order_id,
        %(order_date)s::date as order_date,
        %(order_status)s::varchar as order_status,
        %(product_id)s::varchar as product_id,
        %(brand)s::varchar as brand,
        %(category)s::varchar as category,
        %(sku)s::varchar as sku,
        %(ga_transaction_id)s::varchar as ga_transaction_id,
        %(quantity)s::int as quantity,
        %(price)s::numeric(10,2) as price) orderline
left outer join dim_customer on orderline.customer_id = dim_customer.original_id
left outer join dim_order on orderline.order_id = dim_order.original_id
left outer join dim_order_status on orderline.order_status = dim_order_status.status
left outer join dim_time on orderline.order_date = dim_time.cal_date
left outer join dim_product on orderline.product_id = dim_product.original_id
left outer join dim_brand on orderline.brand = dim_brand.brand
left outer join dim_category on orderline.category = dim_category.category
left outer join dim_sku on orderline.sku = dim_sku.sku
left outer join dim_ga_transaction on orderline.ga_transaction_id = dim_ga_transaction.original_id
on conflict on constraint fact_order_customer_id_order_id_order_status_id_time_id_pro_key do nothing;"""
