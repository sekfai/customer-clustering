import os

from django.conf import settings
import pandas as pd


def download_excel(filename, brand, category, sku):
    writer = pd.ExcelWriter(os.path.join(settings.MEDIA_ROOT, filename))
    brand.to_excel(writer, 'Brand', index=False)
    category.to_excel(writer, 'Category', index=False)
    sku.to_excel(writer, 'Sku', index=False)
    writer.save()
