from bokeh.models import ColumnDataSource
from bokeh.models.widgets import DataTable, TableColumn, NumberFormatter
import pandas as pd

def brand_table(data):
    data = data.head(10)
         
    columns = [
        TableColumn(field='Rank', title='Rank'),
        TableColumn(field='Brand', title='Brand'),
        TableColumn(field='Revenue', title='Revenue', formatter=NumberFormatter(format='0,0.00')),
    ]
    data_table = DataTable(source=ColumnDataSource(data), columns=columns, width=600, height=300)
    data_table.row_headers = False
    data_table.fit_columns = True
    return data_table

def category_table(data):
    data = data.head(10)
         
    columns = [
        TableColumn(field='Rank', title='Rank'),
        TableColumn(field='Category', title='Category'),
        TableColumn(field='Revenue', title='Revenue', formatter=NumberFormatter(format='0,0.00')),
    ]
    data_table = DataTable(source=ColumnDataSource(data), columns=columns, width=600, height=300)
    data_table.row_headers = False
    data_table.fit_columns = True
    return data_table

def sku_table(data):
    data = data.head(10)
         
    columns = [
        TableColumn(field='Rank', title='Rank'),
        TableColumn(field='Sku', title='Sku'),
        TableColumn(field='Quantity', title='Quantity', formatter=NumberFormatter(format='0,0')),
        TableColumn(field='Revenue', title='Revenue', formatter=NumberFormatter(format='0,0.00')),
    ]
    data_table = DataTable(source=ColumnDataSource(data), columns=columns, width=600, height=300)
    data_table.row_headers = False
    data_table.fit_columns = True
    return data_table
