import os

from django.conf import settings

import pandas as pd


def table(data):
    aggregation = {
        'brand': {'num_of_brands': 'count'},
        'unit_per_trans': {'unit_per_trans': 'mean'},
        'spent_per_brand': {'spent_per_brand': 'mean'},
        'order_value': {'order_value': 'mean'},
        'other_brands': {'other_brands': 'mean'},
    }
    summary = data.groupby('label').agg(aggregation)
    summary.columns = summary.columns.droplevel(level=0)
    return summary.reset_index()

def download_excel(filename, data, outliers):
    writer = pd.ExcelWriter(os.path.join(settings.MEDIA_ROOT, filename))
        
    for i in range(data['label'].nunique()):
        sheet_name = 'Group {}'.format(i)
        data[data['label'] == i].to_excel(writer, sheet_name, index=False)
    outliers.to_excel(writer, 'Outliers', index=False)
    writer.save()


def transform(data):
    aggregation = {
        'qty': {'unit_per_trans': 'mean'},
        'subtotal': {'spent_per_brand': 'mean'},
        'order_value': {'order_value': 'mean'},
        'other_brands': {'other_brands': 'mean'},
    }
    summary = data.groupby('brand').agg(aggregation)
    summary.columns = summary.columns.droplevel(level=0)
    return summary.reset_index().round(2)
