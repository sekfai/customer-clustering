import pandas as pd
import numpy as np

from bokeh.models import ColumnDataSource, FixedTicker, HoverTool, Legend
from bokeh.models.widgets import DataTable, TableColumn, NumberFormatter
from bokeh.plotting import figure
from bokeh.palettes import brewer


def chart(data):
    p = figure(width=700, height=500, toolbar_location="below", toolbar_sticky=False)
    item = []
    for i in range(data.label.nunique()):
        source = ColumnDataSource(data[data.label == i])
        group = p.circle(x='x', y='y', fill_color=brewer['Set1'][8][i], fill_alpha=0.7, size=10, source=source)

        p.add_tools(HoverTool(renderers=[group],
                              tooltips=[("Brand", "@brand"),
                                        ("Unit Per Transaction", "@unit_per_trans{0,0.00}"),
                                        ("Spent Per Brand", "@spent_per_brand{0,0.00}"),
                                        ("Order Value", "@order_value{0,0.00}"),
                                        ("Other Brands", "@other_brands{0,0.00}")]))
        item.append(("Group {}".format(i), [group]))

    legend = Legend(items=item, location=(2, 0))

    p.add_layout(legend, 'right')
    p.xaxis[0].ticker=FixedTicker(ticks=[])
    p.yaxis[0].ticker=FixedTicker(ticks=[])
    p.background_fill_color = "beige"
    p.background_fill_alpha = 0.5
    p.title.text_font_size = '12pt'
    p.title.align = 'center'
    p.outline_line_width = 1
    p.outline_line_color = "black"
    return p


def table(data):
    source = ColumnDataSource(data)
    formatter1 = NumberFormatter(format='0,0')
    formatter2 = NumberFormatter(format='0,0.00')
    columns = [
        TableColumn(field="label", title="Group", formatter=formatter1),
        TableColumn(field="num_of_brands", title="Number of Brands", formatter=formatter1),
        TableColumn(field="unit_per_trans", title="Unit Per Transaction", formatter=formatter2),
        TableColumn(field="spent_per_brand", title="Spent Per Brand", formatter=formatter2),
        TableColumn(field="order_value", title="Order Value", formatter=formatter2),
        TableColumn(field="other_brands", title="Other Brands", formatter=formatter2),
    ]
    data_table = DataTable(source=source, columns=columns, width=800, height=200)
    data_table.row_headers = False
    data_table.fit_columns = True
    return data_table
