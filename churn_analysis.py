import os
from django.conf import settings
import pandas as pd


def download_excel(filename, alive, churn):
    columns = ['email','recency','frequency',
               'average_order_value','num_of_transactions','revenue']
    writer = pd.ExcelWriter(os.path.join(settings.MEDIA_ROOT, filename))
    churn[columns].to_excel(writer, 'Churn', index=False)
    alive[columns].to_excel(writer, 'Alive', index=False)
    writer.save()
