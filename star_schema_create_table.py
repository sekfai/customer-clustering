import psycopg2

dim_customer = """
create table dim_customer
(id serial primary key, original_id varchar, email varchar, unique(original_id, email));
"""

dim_order = """
create table dim_order
(id serial primary key, original_id varchar, unique(original_id));
"""

dim_order_status = """
create table dim_order_status
(id serial primary key, status varchar, unique(status));
"""

dim_time = """
create table dim_time (
    id serial primary key,
    year double precision,
    day_of_year double precision,
    quarter double precision,
    month double precision,
    month_name text,
    day_of_month double precision,
    week double precision,
    day_of_week double precision,
    cal_date date,
    unique(cal_date));
"""

dim_product = """
create table dim_product
(id serial primary key, original_id varchar, unique(original_id));
"""

dim_brand = """
create table dim_brand
(id serial primary key, brand varchar, unique(brand));
"""

dim_category = """
create table dim_category
(id serial primary key, category varchar, unique(category));
"""

dim_sku = """
create table dim_sku
(id serial primary key, sku varchar, unique(sku));
"""

dim_ga_transaction = """
create table dim_ga_transaction
(id serial primary key, original_id varchar, unique(original_id));
"""

dim_source = """
create table dim_source
(id serial primary key, source varchar, unique(source));
"""

dim_medium = """
create table dim_medium
(id serial primary key, medium varchar, unique(medium));
"""

dim_campaign = """
create table dim_campaign
(id serial primary key, campaign varchar, unique(campaign));
"""

fact_order = """
create table fact_order (
    customer_id bigint,
    order_id bigint,
    order_status_id bigint,
    time_id bigint,
    product_id bigint,
    brand_id bigint,
    category_id bigint,
    sku_id bigint,
    ga_transaction_id bigint,
    source_id bigint,
    medium_id bigint,
    campaign_id bigint,
    qty int,
    subtotal numeric(10,2),
    unique(customer_id, order_id, order_status_id, time_id, product_id, brand_id, category_id, sku_id, ga_transaction_id));
"""

def create_tables(dsn):
    conn = psycopg2.connect(dsn)
    cur = conn.cursor()
    queries = [
        dim_customer,
        dim_order,
        dim_order_status,
        dim_time,
        dim_product,
        dim_brand,
        dim_category,
        dim_sku,
        dim_ga_transaction,
        dim_source,
        dim_medium,
        dim_campaign,
        fact_order
    ]
    for query in queries:
        cur.execute(query)
        conn.commit()

def drop_tables(dsn):
    conn = psycopg2.connect(dsn)
    cur = conn.cursor()
    tables = [
        'dim_customer',
        'dim_order',
        'dim_order_status',
        'dim_time',
        'dim_product',
        'dim_brand',
        'dim_category',
        'dim_sku',
        'dim_ga_transaction',
        'dim_source',
        'dim_medium',
        'dim_campaign',
        'fact_order'
    ]
    SQL = 'drop table {};'
    for table in tables:
        cur.execute(SQL.format(table))
        conn.commit()

if __name__ == "__main__":
    try:
        # drop_tables('user=ubuntu password=hello123 dbname=db_test')
    except:
        pass
    ##Normal Postgresql database
    # create_tables('user=ubuntu password=hello123 dbname=db_test')

    ##AWS RDS Instance database
