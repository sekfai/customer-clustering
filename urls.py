"""dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

from web import views as web_views
from impersuasion_bi.views import show_online_users


urlpatterns = [
    url(r'^$', web_views.home, name='home'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^api/$', web_views.api, name='api'),
    url(r'^ga_api/$', web_views.ga_api, name='ga_api'),
    url(r'^performance/$', web_views.do_performance, name='performance'),
    url(r'^clustering/$', web_views.do_clustering, name='clustering'),
    url(r'^churn/$', web_views.do_churn, name='churn'),
    url(r'^cohort/$', web_views.do_cohort, name='cohort'),
    url(r'^race_classification/$', web_views.race_classification, name='race_classification'),
    url(r'^brand_clustering/$', web_views.do_brand_clustering, name='brand_clustering'),
    url(r'^new_vs_returning/$', web_views.do_new_vs_returning, name='new_vs_returning'),
    url(r'^new_vs_returning_top_brand/$', web_views.do_new_vs_returning_top_brand, name='new_vs_returning_top_brand'),
    url(r'^new_vs_returning_top_category/$', web_views.do_new_vs_returning_top_category, name='new_vs_returning_top_category'),
    url(r'^new_vs_returning_top_product/$', web_views.do_new_vs_returning_top_product, name='new_vs_returning_top_product'),
    url(r'^admin/', admin.site.urls),
    url(r'^datawarehouse/', include('datawarehouse.urls', namespace='datawarehouse')),
    url(r'^reports/', include('reports.urls', namespace='reports')),
    url(r'^cart_abandonment/', include('cart_abandonment.urls', namespace='cart_abandonment')),
    url(r'^mailchimp/', include('mailchimp.urls', namespace='mailchimp')),
    url(r'^users/online/$', show_online_users, name='show_online_users'),
]

if settings.USE_DJANGO_DEBUG_TOOLBAR:
    import debug_toolbar

    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))

