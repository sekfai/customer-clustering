import pandas as pd
import pygal
from pygal.style import Style

custom_style = Style(
    title_font_size = 15,
    legend_font_size = 10,
    tooltip_font_size = 11,
)


def new_vs_returning(new, returning):
    revenue_of_new = new.revenue.sum()
    revenue_of_returning = returning.revenue.sum()
    total = revenue_of_new + revenue_of_returning
    percentage_revenue_of_new = revenue_of_new / total * 100

    pie_chart = pygal.Pie(height=300, print_values=True, style=custom_style)
    pie_chart.add('New', [{
        'value': percentage_revenue_of_new,
        'formatter': lambda x: '{:.1f}%'.format(x),
        'label':'$ {:,.2f}'.format(revenue_of_new)}])
    pie_chart.add('Returning', [{
        'value': 100 - percentage_revenue_of_new,
        'formatter': lambda x: '{:.1f}%'.format(x),
        'label':'$ {:,.2f}'.format(revenue_of_returning)}])
    
    pie_chart.show_y_labels = False
    return pie_chart.render_data_uri()

def top_brand(data):
    data = data.tail(10)
    bar_chart = pygal.HorizontalStackedBar(height=500, value_formatter=lambda x: '$ {:,}'.format(x), style=custom_style)
    bar_chart.x_labels = data['Brand'].tolist()
    bar_chart.show_x_labels = False # this x refers horizontal axis
    bar_chart.add('Returning', data['Returning'].tolist())
    bar_chart.add('New', data['New'].tolist())
    return bar_chart.render_data_uri()

def top_category(data):
    data = data.tail(10)
    bar_chart = pygal.HorizontalStackedBar(height=500, value_formatter=lambda x: '$ {:,}'.format(x), style=custom_style)
    bar_chart.x_labels = data['Category'].tolist()
    bar_chart.show_x_labels = False
    bar_chart.add('Returning', data['Returning'].tolist())
    bar_chart.add('New', data['New'].tolist())
    return bar_chart.render_data_uri()

def top_product(data):
    data = data.tail(10)
    bar_chart = pygal.HorizontalStackedBar(height=500, value_formatter=lambda x: '$ {:,}'.format(x), style=custom_style)
    bar_chart.x_labels = data['Sku'].tolist()
    bar_chart.show_x_labels = False
    bar_chart.add('Returning', data['Returning'].tolist())
    bar_chart.add('New', data['New'].tolist())
    return bar_chart.render_data_uri()
