import itertools
import json
import psycopg2
import psycopg2.extras
import requests
from datetime import datetime, timedelta
from operator import itemgetter

conn = psycopg2.connect('user=ubuntu password=hello123 dbname=db_test')
dict_cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

SQL = """
select
    customer.id as customer_id,
    customer.email,
    "order".id as order_id,
    date("order".created_at) as order_date,
    "order".status as order_status,
    "order".number as ga_transaction_id,
    product.id as product_id,
    product.brand,
    product.category,
    product.sku,
    order_item.qty as quantity,
    order_item.price
from customer, "order", order_item, product
where
    customer.id = "order".customer_id and
    "order".id = order_item.order_id and
    product.id = order_item.product_id and
    date_part('year', "order".created_at) = %(year)s and
    date_part('month', "order".created_at) = %(month)s and
    date_part('day', "order".created_at) = %(day)s;
"""

# hermo min max dates
# start_date = "2013-09-10"
start_date = "2017-01-1"
stop_date = "2017-07-5"
# stop_date = "2013-09-20"
start = datetime.strptime(start_date, "%Y-%m-%d")
stop = datetime.strptime(stop_date, "%Y-%m-%d")

other_than_200 = 0
fail_dates = []
while start <= stop:
    dict_cur.execute(SQL, ({
        'year': start.year,
        'month': start.month,
        'day': start.day}))
    orderlines = [dict(i) for i in dict_cur.fetchall()] # list of dict
    orderlines = sorted(orderlines, key=itemgetter('order_id'))

    post_data = []
    for order_id, li in itertools.groupby(orderlines, key=itemgetter('order_id')):
        # order level
        order = {'products':[]}
        
        for i in li: # for dict in list(each dict is an orderline)
            for key in ['customer_id', 'email', 'order_id', 'order_status', 'ga_transaction_id']:
                try:
                    order[key] = str(i[key])
                except KeyError:
                    pass

            try:
                order['order_date'] = i['order_date'].strftime('%Y-%m-%d')
            except KeyError:
                pass
        
            i.pop('customer_id')
            i.pop('email')
            i.pop('order_id')
            i.pop('order_date')
            i.pop('order_status')
            i.pop('ga_transaction_id')
            
            try:
                i['quantity'] = str(i['quantity'])
            except KeyError:
                pass

            try:
                i['price'] = str(i['price'])
            except KeyError:
                pass        

            order['products'].append(i)
        post_data.append(order)

    j = json.dumps(post_data)
    print (start.strftime('%Y-%m-%d'))
    print ('len of json:', len(j))

    r = requests.post('http://localhost:8000/api/', data={
        'payload': json.dumps(post_data),
        'username': 'ubuntu',
        'password': 'hello123'})
    print ('status_code:', r.status_code)
    if r.status_code != 200:
        other_than_200 += 1
        fail_dates.append(start.strftime('%Y-%m-%d'))

    start = start + timedelta(days=1)

print ("number of failed api calls:", other_than_200)
print ("fail_dates:", fail_dates)
