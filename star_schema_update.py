fact_order = """
with y as (
    select dim_ga_transaction.id as ga_transaction_id,
           dim_source.id as source_id,
           dim_medium.id as medium_id,
           dim_campaign.id as campaign_id
    from dim_ga_transaction, dim_source, dim_medium, dim_campaign, (
        select %(ga_transaction_id)s::varchar as ga_transaction_id,
               %(source)s::varchar as source,
               %(medium)s::varchar as medium,
               %(campaign)s::varchar as campaign) x
    where x.ga_transaction_id = dim_ga_transaction.original_id and
          x.source = dim_source.source and
          x.medium = dim_medium.medium and
          x.campaign = dim_campaign.campaign)
update fact_order set source_id = y.source_id,
                      medium_id = y.medium_id,
                      campaign_id = y.campaign_id
from y where fact_order.ga_transaction_id = y.ga_transaction_id;"""
