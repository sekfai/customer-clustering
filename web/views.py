from bokeh.embed import components
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError, transaction
from django import http
from django.shortcuts import render
from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
import json
from django.db import connections
import numpy as np
import pandas as pd
from web import forms

import os
import sys


import bokeh_brand_clustering
import bokeh_clustering
import brand_clustering_analysis
import bokeh_performance
import churn_analysis
import clustering_analysis
import clv
import cohort_analysis
import kmeans
import magento_api
import new_vs_returning_analysis
import performance_analysis
import pull_data
import pygal_plot
from waffle.decorators import waffle_flag


def performance_plots(request, start_date, end_date):
    brand = pull_data.brand_sort_by_revenue(request.user.username, start_date, end_date)
    category = pull_data.category_sort_by_revenue(request.user.username, start_date, end_date)
    sku = pull_data.sku_sort_by_revenue(request.user.username, start_date, end_date)

    filename = '{}_performance.xlsx'.format(request.user.username)
    performance_analysis.download_excel(filename, brand, category, sku)

    return {
        'top10_brand': bokeh_performance.brand_table(brand),
        'top10_category': bokeh_performance.category_table(category),
        'top10_sku': bokeh_performance.sku_table(sku),
    }


@waffle_flag('performance')
@login_required
def do_performance(request):
    interval = request.GET.get('interval', '')

    if not interval:
        return render(request, 'performance.html')

    start_date, end_date = interval.split(' - ')
    script, div = components(performance_plots(request, start_date, end_date))

    return render(request, 'performance.html', {
        'div': div,
        'script': script,
        'interval': interval,
    })

def clustering_plots(request, num_of_clusters, start_date, end_date, filename, json_filename):
    data = pull_data.clustering(request.user.username, start_date, end_date)
    first_time, returning = clustering_analysis.split(data)
    
    features = ['recency', 'frequency', 'average_order_value']
    returning = clustering_analysis.determine_outlier(returning, features)
    clustered_data = kmeans.KMeans_clustering(
        returning[returning.outlier == 0], 'email', features, n_clusters=num_of_clusters)
    summary = clustering_analysis.summary(clustered_data)
    outliers = returning[returning.outlier == 1]
    clustering_analysis.download_excel(filename, clustered_data, first_time, outliers)
    
    brand_table = clustering_analysis.compare_top10('brand',
                                                    request.user.username,
                                                    clustered_data,
                                                    start_date,
                                                    end_date)
    category_table = clustering_analysis.compare_top10('category',
                                                       request.user.username,
                                                       clustered_data,
                                                       start_date,
                                                       end_date)
    product_table = clustering_analysis.compare_top10('product',
                                                  request.user.username,
                                                  clustered_data,
                                                  start_date,
                                                  end_date)
    sku_table = clustering_analysis.compare_top10('sku',
                                                  request.user.username,
                                                  clustered_data,
                                                  start_date,
                                                  end_date)
    
    product = product_table.to_json(orient='columns')
    clustering_analysis.download_json(json_filename, product)
    
    stats = {
        'all_customers': len(data),
        'num_of_first_time': len(first_time),
        'num_of_returning': len(returning),
        'num_of_outlier': sum(returning.outlier == 1),
        'num_of_valid_data_point': sum(returning.outlier == 0),
    }

    bokeh = {
        'chart': bokeh_clustering.chart(clustered_data),
        'table': bokeh_clustering.table(summary),
        'brand_table': bokeh_clustering.plot(brand_table),
        'category_table': bokeh_clustering.plot(category_table),
        'product_table': bokeh_clustering.plot(product_table),
        'sku_table': bokeh_clustering.plot(sku_table),
    }
    return [stats, bokeh]

@waffle_flag('clustering')
@login_required
def do_clustering(request):
    interval = request.GET.get('interval', '')
    num_clusters = request.GET.get('num_clusters', '')

    if not interval:
        return render(request, 'clustering.html')

    try:
        num_clusters = int(num_clusters)
    except ValueError:
        num_clusters = 4

    start_date, end_date = interval.split(' - ')
    filename = '{}_clustering.xlsx'.format(request.user.username)
    json_filename = '{}_clustering.json'.format(request.user.username)
    file_url = settings.MEDIA_URL + filename

    stats, bokeh = clustering_plots(request, num_clusters, start_date, end_date, filename, json_filename)
    script, div = components(bokeh)
    action_url = reverse('reports:prepare-excel') \
        + "?source=xlsx&file={}".format(filename)

    
    context = {
        'div': div,
        'script': script,
        'interval': interval,
        'num_clusters': request.GET.get('num_clusters', ''),
        'file_url': file_url,
        'action_url': action_url,
        'set_url': '/reports/fb_login/'
    }
    context.update(stats)
    return render(request, 'clustering.html', context)


def churn_plots(request, start_date, end_date, filename):
    data = pull_data.churn(request.user.username, start_date, end_date)
    first_time, returning = clustering_analysis.split(data)
    features = ['recency', 'frequency']
    
    returning = clustering_analysis.determine_outlier(returning, features)
    clustered_data = kmeans.KMeans_clustering(
        returning[returning.outlier == 0], 'email', features, n_clusters=8)
    summary = clustering_analysis.summary(clustered_data)
    summary['difference'] = summary['recency'] - summary['frequency']
    churn = clustered_data[clustered_data.label == summary.difference.idxmax()]
    alive = data[~data.email.isin(churn.email.tolist())]
    churn_analysis.download_excel(filename, alive, churn)
    return {
        'total_number_of_customers': len(data),
        'number_of_churn': len(churn),
        'number_of_alive': len(alive),
    }


@waffle_flag('churn')
@login_required
def do_churn(request):
    interval = request.GET.get('interval', '')

    if not interval:
        return render(request, 'churn.html')

    start_date, end_date = interval.split(' - ')
    filename = '{}_churn.xlsx'.format(request.user.username)
    stats = churn_plots(request, start_date, end_date, filename)
    file_url = settings.MEDIA_URL + filename
    action_url = reverse('reports:prepare-excel') \
        + "?source=xlsx&file={}".format(filename)

    context = {
        'file_url': file_url,
        'action_url': action_url,
        'interval': interval,
    }

    context.update(stats)
    return render(request, 'churn.html', context)


def cohort_plots(request, cohort_value, cohort_type, imagename, filename):
    fire = {
        'brand': pull_data.brand_cohort,
        'category': pull_data.category_cohort,
    }

    data = fire[cohort_type](request.user.username, cohort_value)
    cohort_analysis.plot_heatmap(imagename, data)
    cohort_analysis.download_excel(filename, data)


@waffle_flag('cohort')
@login_required
def do_cohort(request):
    cohort_value = request.GET.get('cohort_value', '')
    cohort_type = request.GET.get('cohort_type', '')

    if not cohort_value:
        return render(request, 'cohort.html')
    
    imagename = '{}_cohort.png'.format(request.user.username)
    try:
        os.remove(os.path.join(settings.MEDIA_ROOT, imagename))
    except OSError:
        pass

    filename = '{}_cohort.xlsx'.format(request.user.username)
    cohort_plots(request, cohort_value, cohort_type, imagename, filename)
    file_url = settings.MEDIA_URL + filename
    img_url = settings.MEDIA_URL + imagename

    return render(request, 'cohort.html', {
        'cohort_value': cohort_value,
        'img_url': img_url,
        'file_url': file_url,
    })


@waffle_flag('new_vs_returning')
@login_required
def do_new_vs_returning(request):
    interval = request.GET.get('interval', '')

    if not interval:
        return render(request, 'new_vs_returning.html')

    tab_name = "Customer and Revenue"
    filename = "{}_new_vs_returning.xlsx".format(request.user.username)
    start_date, end_date = interval.split(' - ')

    data = pull_data.new_vs_returning(request.user.username, start_date, end_date)
    new = data[data.customer_status == 'New']
    returning = data[data.customer_status == 'Returning']
    chart = pygal_plot.new_vs_returning(new, returning)
    new_vs_returning_analysis.download_excel(filename, new, returning)
    action_url = reverse('reports:prepare-excel') \
        + "?source=xlsx&file={}".format(filename)

    return render(request, 'new_vs_returning.html', {
        'tab_name': tab_name,
        'filename': filename,
        'chart': chart,
        'interval': interval,
        'num_of_new': len(new),
        'num_of_returning': len(returning),
        'action_url': action_url,
    })


@waffle_flag('new_vs_returning_top_brand')
@login_required
def do_new_vs_returning_top_brand(request):
    interval = request.GET.get('interval', '')

    if not interval:
        return render(request, 'new_vs_returning.html')

    tab_name = "Top 10 Brands"
    file_name = "{}_new_vs_returning_top_brand.csv".format(request.user.username)
    start_date, end_date = interval.split(' - ')

    data = pull_data.new_vs_returning_top_brand(request.user.username, start_date, end_date)
    pivot_table = new_vs_returning_analysis.brand_pivot_table(data)
    chart = pygal_plot.top_brand(pivot_table)
    new_vs_returning_analysis.download_csv(file_name, pivot_table)

    return render(request, 'new_vs_returning.html', {
        'tab_name': tab_name,
        'file_name': file_name,
        'chart': chart,
        'interval': interval,
    })


@waffle_flag('new_vs_returning_top_category')
@login_required
def do_new_vs_returning_top_category(request):
    interval = request.GET.get('interval', '')

    if not interval:
        return render(request, 'new_vs_returning.html')

    tab_name = "Top 10 Categories"
    file_name = "{}_new_vs_returning_top_category.csv".format(request.user.username)
    start_date, end_date = interval.split(' - ')

    data = pull_data.new_vs_returning_top_category(request.user.username, start_date, end_date)
    pivot_table = new_vs_returning_analysis.category_pivot_table(data)
    chart = pygal_plot.top_category(pivot_table)
    new_vs_returning_analysis.download_csv(file_name, pivot_table)

    return render(request, 'new_vs_returning.html', {
        'tab_name': tab_name,
        'file_name': file_name,
        'chart': chart,
        'interval': interval,
    })


@waffle_flag('new_vs_returning_top_product')
@login_required
def do_new_vs_returning_top_product(request):
    interval = request.GET.get('interval', '')

    if not interval:
        return render(request, 'new_vs_returning.html')

    tab_name = "Top 10 Products"
    file_name = "{}_new_vs_returning_top_product.csv".format(request.user.username)
    start_date, end_date = interval.split(' - ')

    data = pull_data.new_vs_returning_top_product(request.user.username, start_date, end_date)
    pivot_table = new_vs_returning_analysis.product_pivot_table(data)
    chart = pygal_plot.top_product(pivot_table)
    new_vs_returning_analysis.download_csv(file_name, pivot_table)

    return render(request, 'new_vs_returning.html', {
        'tab_name': tab_name,
        'file_name': file_name,
        'chart': chart,
        'interval': interval,
    })


def brand_clustering_plots(request, num_clusters, start_date, end_date, filename):
    
    data = pull_data.brand_clustering(request.user.username, start_date, end_date)
    variables = ['qty', 'subtotal', 'order_value', 'other_brands']
    data = clustering_analysis.determine_outlier(data, variables)
    outlier_order_id = data[data.outlier == 1].order_id.tolist()
    data = data[~data.order_id.isin(outlier_order_id)]
    df = brand_clustering_analysis.transform(data)

    features = ['unit_per_trans', 'spent_per_brand', 'order_value', 'other_brands']
    df = clustering_analysis.determine_outlier(df, features)
    clustered_data = kmeans.KMeans_clustering(df[df.outlier == 0], 'brand', features, num_clusters)
    outliers = df[df.outlier == 1]
    brand_clustering_analysis.download_excel(filename, clustered_data, outliers)
    table = brand_clustering_analysis.table(clustered_data)
    stats = {
        'num_of_brands': len(df),
        'num_of_valid': len(df) - len(outliers),
        'num_of_outliers': len(outliers),
    }

    bokeh = {
        'chart': bokeh_brand_clustering.chart(clustered_data),
        'table': bokeh_brand_clustering.table(table),
    }
    
    return [stats, bokeh]


@waffle_flag('brand_clustering')
@login_required
def do_brand_clustering(request):
    interval = request.GET.get('interval', '')
    num_clusters = request.GET.get('num_clusters', '')

    if not interval:
        return render(request, 'brand_clustering.html')

    try:
        num_clusters = int(num_clusters)
    except ValueError:
        num_clusters = 4

    start_date, end_date = interval.split(' - ')
    filename = '{}_brand_clustering.xlsx'.format(request.user.username)
    file_url = settings.MEDIA_URL + filename
    stats, bokeh = brand_clustering_plots(request, num_clusters, start_date, end_date, filename)
    script, div = components(bokeh)
    context = {
        'div': div,
        'script': script,
        'interval': interval,
        'num_clusters': request.GET.get('num_clusters', ''),
        'file_url': file_url,
    }
    context.update(stats)
    return render(request, 'brand_clustering.html', context)


def home(request):
    if not request.user.is_authenticated:
        return mainpage(request)
    return render(request, 'base.html')


def race_classification(request):
    name = request.GET.get('name', '')

    if not name:
        return render(request, 'race_classification.html')
    
    import predict

    return render(request, 'race_classification.html', {
        'name': name,
        'race': predict.race(name),
    })


def mainpage(request):
    return render(request, 'mainpage.html')

@csrf_exempt
def api(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is None:
            return http.HttpResponseBadRequest('login error')
        else:
            login(request, user)
            json_data = request.POST.get('payload')
            data = json.loads(json_data)
            order_lines = []
            for order in data:
                # make orderlines from order,output products(list of dicts(orderlines))
                products = order.get('products', [])
                order.pop('products')
                for product in products:
                    product.update(order)
                order_lines += products
            
            # update dim_tables and fact_table
            import star_schema_insert_into
            with connections[user.username].cursor() as cursor:
                for orderline in order_lines:
                    if orderline['customer_id'] and \
                       orderline['email'] and \
                       orderline['order_id'] and \
                       orderline['order_date'] and \
                       orderline['order_status'] and \
                       orderline['product_id'] and \
                       orderline['quantity'] and \
                       orderline['price']:
                        cursor.execute(star_schema_insert_into.dim_customer,({
                            'original_id': orderline['customer_id'],
                            'email': orderline['email']}))
                        cursor.execute(star_schema_insert_into.dim_order, (orderline['order_id'],))
                        cursor.execute(star_schema_insert_into.dim_order_status, (orderline['order_status'],))
                        cursor.execute(star_schema_insert_into.dim_time, ({'order_date': orderline['order_date']}))
                        cursor.execute(star_schema_insert_into.dim_product, (orderline['product_id'],))

                        if orderline['brand']:
                            cursor.execute(star_schema_insert_into.dim_brand, (orderline['brand'],))
                        
                        if orderline['category']:
                            cursor.execute(star_schema_insert_into.dim_category, (orderline['category'],))

                        if orderline['sku']:
                            cursor.execute(star_schema_insert_into.dim_sku, (orderline['sku'],))
                        
                        if orderline['ga_transaction_id']:
                            cursor.execute(star_schema_insert_into.dim_ga_transaction, (orderline['ga_transaction_id'],))
                        
                        cursor.execute(star_schema_insert_into.fact_order, ({
                            'customer_id': orderline['customer_id'],
                            'order_id': orderline['order_id'],
                            'order_status': orderline['order_status'],
                            'order_date': orderline['order_date'],
                            'product_id': orderline['product_id'],
                            'brand': orderline['brand'],
                            'category': orderline['category'],
                            'sku': orderline['sku'],
                            'ga_transaction_id': orderline['ga_transaction_id'],
                            'quantity': float(orderline['quantity']),
                            'price': orderline['price']}))
            return http.HttpResponse('insert successfully')
    else:
        return http.HttpResponseBadRequest('Only POST is supported')


@csrf_exempt
def ga_api(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is None:
            return http.HttpResponseBadRequest('login error')
        else:
            login(request, user)
            json_data = request.POST.get('payload')
            data = json.loads(json_data)
            import star_schema_insert_into
            import star_schema_update
            with connections[user.username].cursor() as cursor:
                for ga_transaction in data:
                    cursor.execute(star_schema_insert_into.dim_source, (ga_transaction['source'],))
                    cursor.execute(star_schema_insert_into.dim_medium, (ga_transaction['medium'],))
                    cursor.execute(star_schema_insert_into.dim_campaign, (ga_transaction['campaign'],))
                    cursor.execute(star_schema_update.fact_order, ({
                        'ga_transaction_id': ga_transaction['ga_transaction_id'],
                        'source': ga_transaction['source'],
                        'medium': ga_transaction['medium'],
                        'campaign': ga_transaction['campaign'],
                    }))
            return http.HttpResponse('update successfully')
    else:
        return http.HttpResponseBadRequest('Only POST is supported')
