from django import forms


class ClusteringForm(forms.Form):
    num_of_clusters = forms.IntegerField()
    start_date = forms.DateField()
    end_date = forms.DateField()


class ChurnForm(forms.Form):
    start_date = forms.DateField()
    end_date = forms.DateField()
    percentage = forms.IntegerField(max_value=90, min_value=50)


class CohortForm(forms.Form):
    sku = forms.CharField()
