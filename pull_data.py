from django import db
import pandas as pd

order_status = {
    'client_x': ('RECEIVED', 'POSTED'),
    'tbs': ('erp_confirmed','erp_confirmed'),
}

def churn(username, start_date, end_date):
    from queries.churn import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })

def clustering(username, start_date, end_date):
    from queries.clustering import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })

def new_vs_returning(username, start_date, end_date):
    from queries.new_vs_returning import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })

def new_vs_returning_top_brand(username, start_date, end_date):
    from queries.new_vs_returning.brand import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })

def new_vs_returning_top_category(username, start_date, end_date):
    from queries.new_vs_returning.category import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })

def new_vs_returning_top_product(username, start_date, end_date):
    from queries.new_vs_returning.sku import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })

def brand_cohort(username, brand):
    from queries.cohort.brand import query
    return pd.read_sql_query(query, db.connections[username], params={
        'brand': brand,
        'order_status': order_status[username],
    })

def category_cohort(username, category):
    from queries.cohort.category import query
    return pd.read_sql_query(query, db.connections[username], params={
        'category': category,
        'order_status': order_status[username],
    })

def brand_clustering(username, start_date, end_date):
    from queries.brand_clustering import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })

def brand_sort_by_revenue(username, start_date, end_date):
    from queries.performance.brand import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })

def category_sort_by_revenue(username, start_date, end_date):
    from queries.performance.category import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })

def sku_sort_by_revenue(username, start_date, end_date):
    from queries.performance.sku import query
    return pd.read_sql_query(query, db.connections[username], params={
        'start_date': start_date,
        'end_date': end_date,
        'order_status': order_status[username],
    })
