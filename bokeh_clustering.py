import pandas as pd
import numpy as np

from bokeh.models import ColumnDataSource, FixedTicker, HoverTool, Legend
from bokeh.models.widgets import DataTable, TableColumn, NumberFormatter
from bokeh.plotting import figure
from bokeh.palettes import brewer


def chart(data):
    p = figure(width=700, height=500, toolbar_location=None)
    item = []
    for i in range(data.label.nunique()):
        source = ColumnDataSource(data[data.label == i])
        group = p.circle(x='x', y='y', fill_color=brewer['Set1'][8][i], fill_alpha=0.7, size=10, source=source)

        p.add_tools(HoverTool(renderers=[group],
                              tooltips=[("Email", "@email"),
                                        ("Recency", "@recency{0.00} days"),
                                        ("Frequency", "@frequency{0.00} days"),
                                        ("Monetary Value", "$ @average_order_value{0.00}")]))

        item.append(("Group {}".format(i), [group]))

    legend = Legend(items=item, location=(2, 10))

    p.add_layout(legend, 'right')
    p.xaxis[0].ticker=FixedTicker(ticks=[])
    p.yaxis[0].ticker=FixedTicker(ticks=[])
    p.background_fill_color = "beige"
    p.background_fill_alpha = 0.5
    p.outline_line_width = 1
    p.outline_line_color = "black"
    return p

def table(data):
    source = ColumnDataSource(data)
    formatter = NumberFormatter(format='0,0.00')
    columns = [
        TableColumn(field="label", title="Group"),
        TableColumn(field="customer_id", title="Number of Customers"),
        TableColumn(field="recency", title="Recency", formatter=formatter),
        TableColumn(field="frequency", title="Frequency", formatter=formatter),
        TableColumn(field="average_order_value", title="Average Order Value", formatter=formatter),
        TableColumn(field="num_of_transactions", title="Number of Transactions", formatter=formatter),
        TableColumn(field="revenue", title="Average Revenue", formatter=formatter),
        TableColumn(field="total_revenue", title="Total Revenue", formatter=formatter),
    ]
    data_table = DataTable(source=source, columns=columns, width=850, height=200)
    data_table.row_headers = False
    data_table.fit_columns = True
    return data_table

def plot(data):
    source = ColumnDataSource(data)
    columns = []
    for col in data.columns.tolist():
        columns += [TableColumn(field=col, title=col)]
    data_table = DataTable(source=source, columns=columns, width=800, height=300)
    data_table.row_headers = False
    data_table.fit_columns = True
    return data_table
