import os
import numpy as np

from django import db
from django.conf import settings
import pandas as pd
import json
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
order_status = {
    'client_x': ('RECEIVED', 'POSTED'),
    'tbs': ('erp_confirmed', 'erp_confirmed'),
}

def summary(data):
    aggregation = {
        'customer_id': 'count',
        'recency': 'mean',
        'frequency': 'mean',
        'average_order_value': 'mean',
        'num_of_transactions': 'mean',
        'revenue': 'mean',
    }
    summary = data.groupby('label').agg(aggregation)
    summary['total_revenue'] = summary['customer_id'] * summary['revenue']
    return summary

def download_excel(filename, data, first_time, outliers):
    writer = pd.ExcelWriter(os.path.join(settings.MEDIA_ROOT, filename))
        
    for i in range(data['label'].nunique()):
        sheet_name = 'Group {}'.format(i)
        data[data['label'] == i].to_excel(writer, sheet_name, index=False)
    first_time.to_excel(writer, 'First-time Customers', index=False)
    outliers.to_excel(writer, 'Outliers', index=False)
    writer.save()

def download_json(filename, data):
    path = os.path.join(settings.MEDIA_ROOT, filename)
    if default_storage.exists(path):
        default_storage.delete(path)
    path = default_storage.save(path, ContentFile(data))
    print(default_storage.open(path).read())


def compare_top10(dimension, username, clustered_data, start_date, end_date):
    # dimension can be 'brand', 'category', 'product', 'sku'
    if dimension == 'brand':
        from queries.clustering.compare_top10_brand import query
    elif dimension == 'category':
        from queries.clustering.compare_top10_category import query
    elif dimension == 'product':
        from queries.clustering.compare_top10_product import query
    elif dimension == 'sku':
        from queries.clustering.compare_top10_sku import query
    else:
        return
    from functools import reduce

    list_of_top10 = []
    for i in range(clustered_data.label.nunique()):
        ids = tuple(clustered_data[clustered_data.label == i].customer_id.tolist()[:300])
        list_of_top10.append(pd.read_sql_query(query, db.connections[username], params={
            'label': i,
            'customer_ids': ids,
            'start_date': start_date,
            'end_date': end_date,
            'order_status': order_status[username],
        }))
    comparison_table = reduce(lambda left, right: pd.merge(left, right, on='Rank'), list_of_top10)
    return comparison_table

def split(data):
    first_time = data[data.frequency == 0]
    returning = data[data.frequency != 0]
    return first_time, returning

def determine_outlier(returning, features):
    returning['outlier'] = 0
    for feature in features:
        q75, q25 = np.percentile(returning[feature], [75 ,25])
        iqr = q75 - q25

        min = q25 - (iqr*1.5)
        max = q75 + (iqr*1.5)
        returning.loc[returning[feature] < min, 'outlier'] = 1
        returning.loc[returning[feature] > max, 'outlier'] = 1
    return returning
