query = """
select row_number() over (order by "Revenue" desc) as "Rank", x.*
from (
    select
        dim_brand.brand as "Brand",
        round(sum(subtotal), 2) as "Revenue"
    from fact_order, dim_brand, dim_time, dim_order_status
    where
        dim_brand.id = fact_order.brand_id and
        dim_time.id = fact_order.time_id and
        dim_order_status.id = fact_order.order_status_id and
        dim_order_status.status in %(order_status)s and
        dim_time.cal_date between %(start_date)s and %(end_date)s
    group by dim_brand.brand) x;
"""
