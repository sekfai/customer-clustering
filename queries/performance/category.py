query = """
select row_number() over (order by "Revenue" desc) as "Rank", x.*
from (
    select
        dim_category.category as "Category",
        round(sum(subtotal), 2) as "Revenue"
    from fact_order, dim_category, dim_time, dim_order_status
    where
        dim_category.id = fact_order.category_id and
        dim_time.id = fact_order.time_id and
        dim_order_status.id = fact_order.order_status_id and
        dim_order_status.status in %(order_status)s and
        dim_time.cal_date between %(start_date)s and %(end_date)s
    group by dim_category.category) x;
"""
