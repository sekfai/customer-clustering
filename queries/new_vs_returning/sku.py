query = """
select sku as "Sku", customer_status, sum(subtotal) as revenue
from fact_order, dim_order_status, dim_time, dim_sku, (
    select customer_id,
        case when count(distinct cal_date) = 1 then 'New' else 'Returning'
        end as customer_status
    from fact_order, dim_order_status, dim_time
    where
        dim_order_status.id = fact_order.order_status_id and
        dim_time.id = fact_order.time_id and
        dim_order_status.status in %(order_status)s
    group by fact_order.customer_id) x
where
    dim_order_status.id = fact_order.order_status_id and
    dim_time.id = fact_order.time_id and
    dim_sku.id = fact_order.sku_id and
    x.customer_id = fact_order.customer_id and
    dim_order_status.status in %(order_status)s and
    dim_time.cal_date between %(start_date)s and %(end_date)s
group by dim_sku.sku, customer_status;
"""
