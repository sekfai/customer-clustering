query = """
select email, x.customer_status, y.revenue
from dim_customer, (
    select customer_id,
        case when count(distinct cal_date) = 1 then 'New' else 'Returning'
        end as customer_status
    from fact_order, dim_order_status, dim_time
    where
        dim_order_status.id = fact_order.order_status_id and
        dim_time.id = fact_order.time_id and
        dim_order_status.status in %(order_status)s
    group by fact_order.customer_id) x, (

        select customer_id, sum(subtotal) as revenue
        from fact_order, dim_order_status, dim_time
        where
            dim_order_status.id = fact_order.order_status_id and
            dim_time.id = fact_order.time_id and
            dim_order_status.status in %(order_status)s and
            dim_time.cal_date between %(start_date)s and %(end_date)s
        group by fact_order.customer_id) y
where
    x.customer_id = dim_customer.id and
    y.customer_id = dim_customer.id;
"""
