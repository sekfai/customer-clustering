query = """
select z.*, row_number() over (partition by first_purchase_month order by order_month) as cohort_period
from (
    select first_purchase_month, order_month, count(customer_id) as num_of_customers
    from (
        select x.*, first_value(order_month) over (partition by customer_id order by order_month) as first_purchase_month
        from (
            select distinct customer_id, date_trunc('month', cal_date) as order_month
            from fact_order, dim_order_status, dim_time, dim_brand
            where
                dim_order_status.id = fact_order.order_status_id and
                dim_time.id = fact_order.time_id and
                dim_brand.id = fact_order.brand_id and 
                dim_order_status.status in %(order_status)s and
                lower(dim_brand.brand) = lower(%(brand)s)) x) y
    group by first_purchase_month, order_month) z
where
    date_part('year', first_purchase_month) >= 2017 and
    date_part('month', first_purchase_month) >= 1;
"""
