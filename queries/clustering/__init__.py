query = """
select email, z.* from dim_customer, (
    select
        customer_id,
        current_date - max(cal_date) as recency,
        case
            when count(cal_date) = 1 then 0
            else (max(cal_date) - min(cal_date)) / (count(cal_date) - 1)
        end as frequency,
        round(avg(order_value), 2) as average_order_value,
        count(cal_date) as num_of_transactions,
        sum(order_value) as revenue
    from (
        select x.customer_id, cal_date, sum(subtotal) as order_value 
        from fact_order, dim_order_status, dim_time, (
            select distinct customer_id
            from fact_order, dim_order_status, dim_time
            where 
                dim_order_status.id = fact_order.order_status_id and
                dim_time.id = fact_order.time_id and
                dim_time.cal_date between %(start_date)s and %(end_date)s and
                dim_order_status.status in %(order_status)s
            ) x
        where
            x.customer_id = fact_order.customer_id and
            dim_order_status.id = fact_order.order_status_id and
            dim_time.id = fact_order.time_id and
            dim_order_status.status in %(order_status)s
        group by x.customer_id, cal_date) y
    group by customer_id) z
where dim_customer.id = z.customer_id;
"""
