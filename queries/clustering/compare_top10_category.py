query = """
select row_number() over (order by revenue desc) as "Rank", x.category as "Cluster %(label)s" from (
    select category, sum(subtotal) as revenue
    from fact_order, dim_order_status, dim_time, dim_category
    where
        fact_order.customer_id in %(customer_ids)s and
        dim_order_status.id = fact_order.order_status_id and
        dim_time.id = fact_order.time_id and
        dim_category.id = fact_order.category_id and
        dim_order_status.status in %(order_status)s and
        dim_time.cal_date between %(start_date)s and %(end_date)s
    group by dim_category.category) x
limit 10;
"""
