query = """
select
    y.order_id, brand, qty, subtotal, order_value, other_brands
from (
    select
        order_id,
        sum(subtotal) as order_value,
        count(distinct brand_id) - 1 as other_brands
    from fact_order
    group by order_id) x, (

    select
        order_id,
        brand,
        sum(qty) as qty,
        sum(subtotal) as subtotal
    from fact_order, dim_order_status, dim_time, dim_brand
    where
        dim_order_status.id = fact_order.order_status_id and
        dim_time.id = fact_order.time_id and
        dim_brand.id = fact_order.brand_id and
        dim_order_status.status in %(order_status)s and
        dim_time.cal_date between %(start_date)s and %(end_date)s
    group by order_id, brand) y
where
    x.order_id = y.order_id;
"""
