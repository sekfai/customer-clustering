import json
import requests
import psycopg2
import psycopg2.extras

conn = psycopg2.connect('dbname=db_test user=ubuntu password=hello123')
dict_cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

SQL = """
select
    "transactionId" as ga_transaction_id,
    source,
    medium,
    campaign
from ga.report;
"""

dict_cur.execute(SQL)
num_of_api_call = 0
other_than_200 = 0
while True:
    api_format = [dict(row) for row in dict_cur.fetchmany(1000)]
    if api_format:
        r = requests.post('http://localhost:8000/ga_api/', data={
            'payload': json.dumps(api_format),
            'username': 'ubuntu',
            'password': 'hello123'
        })
        num_of_api_call += 1
        print('API CALL: {}'.format(num_of_api_call))
        print(r.status_code)
        if r.status_code != 200:
            other_than_200 += 1
    else:
        break

print ("number of failed api calls:", other_than_200)
