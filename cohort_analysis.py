import os
from django.conf import settings
import pandas as pd


def plot_heatmap(imagename, data):
    data['First Purchase Month'] = data.first_purchase_month.apply(lambda x: x.strftime('%Y-%m'))
    hmap = data.pivot(index='First Purchase Month', columns='cohort_period', values='num_of_customers')
    hmap = hmap.fillna(value=0).astype(int)
    import seaborn as sns
    ax = sns.heatmap(hmap, linewidths=.5, annot=True, fmt="")
    ax.set_xlabel('')
    ax.set_yticklabels(ax.get_yticklabels(), rotation = 0, fontsize = 8)
    fig = ax.get_figure()
    fig.savefig(os.path.join(settings.MEDIA_ROOT, imagename))

def download_excel(filename, data):
    data.first_purchase_month = data.first_purchase_month.dt.strftime('%Y-%m')
    data.order_month = data.order_month.dt.strftime('%Y-%m')
    data = data[['first_purchase_month', 'order_month', 'num_of_customers']]
    writer = pd.ExcelWriter(os.path.join(settings.MEDIA_ROOT, filename))
    data.to_excel(writer, 'cohort', index=False)
    writer.save()
